class TweetsController < ApplicationController
  def index
    render json: Tweet.by_users(user_params[:user_username])
  end

  private

  def user_params
    params.permit(:user_username)
  end
end
