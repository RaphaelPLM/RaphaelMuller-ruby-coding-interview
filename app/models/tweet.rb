class Tweet < ApplicationRecord
  belongs_to :user

  scope :by_users, ->(ids) { joins(:user).where(users: {id: ids}) if ids.present? }
end
