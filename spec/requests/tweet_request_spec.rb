require 'rails_helper'

RSpec.describe 'Tweets', type: :request do
  RSpec.shared_context 'with multiple users' do
    let!(:user_1) { create(:user) }
    let!(:user_2) { create(:user) }

    before do
      5.times do
        create(:tweet, user: user_1)
      end
      5.times do
        create(:tweet, user: user_2)
      end
    end
  end

  describe '#index' do
    let(:result) { JSON.parse(response.body) }

    context 'when fetching tweets by user' do
      include_context 'with multiple users'

      it 'returns only the tweets for the specified user' do
        get user_tweets_path(user_1)

        expect(result.size).to eq(user_1.tweets.size)
        expect(result.map { |element| element['id'] }).to eq(user_1.tweets.ids)
      end
    end

    context 'when fetching all tweets' do
      include_context 'with multiple users'

      it 'returns all the tweets' do
        get tweets_path

        expect(result.size).to eq(Tweet.count)
      end
    end
  end
end
